﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class LockerManager : MonoBehaviour
{
    [SerializeField] private Image[] lockerIcons;
    [SerializeField] private PlayerDatas playerDatas;
    [SerializeField] private Text playerName;

    private void SetName() => playerName.text = playerDatas.playerName;
    private void CheckSelectedIcons()
    {
        for (int i = 0; i < lockerIcons.Length; i++)
        {
            if (lockerIcons[i].sprite == playerDatas.playerIcon[playerDatas.playerIconID])
            {
                lockerIcons[i].GetComponent<Outline>().enabled = true;
            }
            else
            {
                lockerIcons[i].GetComponent<Outline>().enabled = false;
            }
        }
    }

    public void EnableLocker()
    {
        SetName();
        CheckSelectedIcons();
    }
}
