﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityAtoms.BaseAtoms;
using UnityEngine.UI;
using TMPro;

public class VisualFeedbackManager : MonoBehaviour
{
    [SerializeField] private Material ballMaterial;
    [SerializeField] private GameObject bigBallLine;

    [SerializeField] private RectTransform imageP1;
    [SerializeField] private RectTransform imageP2;
    [SerializeField] private IntVariable numberPlayer;
    [SerializeField] private TextMeshProUGUI feedbackText;
    [SerializeField] private Ease textEase;
    private Vector3 bigScale = new Vector3(1.2f, 1.2f, 1.2f);
    private Vector3 littleScale = new Vector3(.8f, .8f, .8f);

    private Tween tweenText;
    private Tween tweenColor;
    private void Start()
    {
        bigBallLine.gameObject.SetActive(false);
        ballMaterial.DOColor(new Color(1, 1, 1, .3f), .1f);
    }

    void Update()
    {

        if(Input.touchCount > 0)
        {
            if(Input.GetTouch(0).phase == TouchPhase.Began)
            {
                bigBallLine.gameObject.SetActive(true);
                ballMaterial.DOColor(new Color(1, 1, 1, 1f), 1);
            }
            else if (Input.GetTouch(0).phase == TouchPhase.Ended)
            {
                bigBallLine.gameObject.SetActive(false);
                ballMaterial.DOColor(new Color(1, 1, 1, .3f), 1);
            }

        }

      
    }

    public void ChangeScaleImagePlayer()
    {
        
            if (numberPlayer.Value == 1)
            {
                imageP1.DOScale(bigScale, .5f);
                imageP2.DOScale(littleScale, .5f);
            }
            else if (numberPlayer.Value == -1)
            {
                imageP2.DOScale(bigScale, .5f);
                imageP1.DOScale(littleScale, .5f);
            }
        
    }

    public void DisplayText(string text)
    {
        tweenColor.Kill();
        tweenText.Kill();
        feedbackText.GetComponent<RectTransform>().localPosition = Vector3.zero;
        feedbackText.text = text;
        feedbackText.GetComponent<RectTransform>().DOLocalMoveY(500f, 2f).SetEase(textEase);
        feedbackText.DOColor(new Color(Color.white.r, Color.white.g, Color.white.b, 1),.5f) ;


        StartCoroutine(EndTween());
        
        
    }

 

    public void DisplayText(string text, Vector3 position)
    {
        tweenColor.Complete();
        tweenText.Complete();
        feedbackText.GetComponent<RectTransform>().localPosition = position;
        feedbackText.text = text;
        tweenText = feedbackText.GetComponent<RectTransform>().DOLocalMoveY(500f, 2f).SetEase(textEase);
        tweenColor = feedbackText.DOColor(new Color(Color.white.r, Color.white.g, Color.white.b, 1), .5f);


        StartCoroutine(EndTween());


    }

    IEnumerator EndTween()
    {
        yield return new WaitForSeconds(1f);
        feedbackText.DOColor(new Color(Color.white.r, Color.white.g, Color.white.b, 0), 1.5f);
        yield return new WaitForSeconds(1.1f);
        feedbackText.GetComponent<RectTransform>().localPosition = Vector3.zero;
        yield break;
    }

}
