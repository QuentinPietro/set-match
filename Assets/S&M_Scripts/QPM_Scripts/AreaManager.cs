﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityAtoms.BaseAtoms;
using Photon.Pun;
using UnityEngine.UI;
public class AreaManager : MonoBehaviour
{
    [SerializeField] private IntVariable turnValue;

    [SerializeField] private GameObject colliderTerrainPlayer1;
    [SerializeField] private GameObject colliderTerrainPlayer2;
    [SerializeField] private GameObject[] colliderTerrainPlayer1Aces;
    [SerializeField] private GameObject[] colliderTerrainPlayer2Aces;
    [SerializeField] private Transform littleBallTrans;
    [SerializeField] private Text counterText;
    [SerializeField] private CounterManager counterManager;
    [SerializeField] private IntVariable counterValue;
    [SerializeField] private IntVariable currentNumberOfExchanges;
    [SerializeField] private BoolVariable isFirstService;
    [SerializeField] private BoolVariable inTraining;

    public PhotonView photonView;

    public bool isBallIn = false;

    [SerializeField] private UnityEngine.Events.UnityEvent _OnShootIn;
    [SerializeField] private UnityEngine.Events.UnityEvent _OnShootOut;
    [SerializeField] private UnityEngine.Events.UnityEvent _OnServiceMissed;

    private Ray rayArea;
    private RaycastHit hit;
    

    private void Start()
    {
        photonView = GetComponent<PhotonView>();
    }

    private void Update()
    {
        if (counterText != null)
        counterText.text = counterValue.Value.ToString();
    }

    
    public void CheckAreas(int counter)
    {
        rayArea.origin = new Vector3(littleBallTrans.position.x, 25, littleBallTrans.position.z);
        rayArea.direction = -littleBallTrans.up;

        if (Physics.Raycast(rayArea, out hit, 50f))
        {
            Debug.Log("Hello");
            if (hit.collider.GetComponent<AreasContainer>())
            {
                Debug.Log("Area IN");
                _OnShootIn.Invoke();
                if (!inTraining.Value)
                    counterManager.ChangeCounterValue(hit.collider.GetComponent<AreasContainer>().value, counter);

            }
            else
            {
                if (!inTraining.Value)
                {
                    if (currentNumberOfExchanges.Value == 0 && isFirstService.Value)
                    {
                        _OnServiceMissed.Invoke();
                    }
                    else
                    {
                        _OnShootOut.Invoke();
                    }
                }
                else
                {
                    Debug.Log("Area OUT");
                    _OnShootOut.Invoke();
                }
                
                
            }
           
        }
        else if (!RaycastManager.isSurfaceTouched<AreasContainer>(rayArea, hit))
        {

            if (!inTraining.Value)
            {
                if (currentNumberOfExchanges.Value == 0 && isFirstService.Value)
                {
                    _OnServiceMissed.Invoke();
                }
                else
                {
                    _OnShootOut.Invoke();
                }
            }
            else
            {
                _OnShootOut.Invoke();
            }

        }
    }

    public void ChangeTerrainCollidersState()
    {
        if (turnValue.Value == 1) { colliderTerrainPlayer1.SetActive(false); colliderTerrainPlayer2.SetActive(true); }
        else { colliderTerrainPlayer1.SetActive(true); colliderTerrainPlayer2.SetActive(false); }
    }

    public void ChangeAceState(bool value)
    {
        for (int i = 0; i < colliderTerrainPlayer1Aces.Length; i++)
        {
            colliderTerrainPlayer1Aces[i].SetActive(value);
            colliderTerrainPlayer2Aces[i].SetActive(value);
        }
    }

}
