﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
using UnityEngine.EventSystems;
public class LauncherManager : MonoBehaviourPunCallbacks
{

    [SerializeField] private PlayerDatas playerDatas;
    [SerializeField] private PlayerDatas playerDatasP2;
    [SerializeField] private GameObject waiting;
    [SerializeField] private GameObject validate;
    [SerializeField] private GameObject launcher;
    [SerializeField] private GameObject mainMenu;
    private GameObject caller;

    private void Start()
    {
        if (playerDatas.isConnected)
        {
            launcher.SetActive(false);
            mainMenu.SetActive(true);
        }
        else
        {
            playerDatas.playerName = "";
            playerDatas.isConnected = true;

        }
        

    }
    public void ChangeNamePlayer(string name)
    {
        PlayerPrefs.SetString("NickName", name);
        playerDatas.playerName = name;
        PhotonNetwork.NickName = name;
        if ( playerDatas.playerName != "")
        {
            validate.SetActive(true);
            waiting.SetActive(false);
        }
        else
        {
            validate.SetActive(false);
            waiting.SetActive(true);
        }
    }

    public void ChangeNamePlayerP2(string name)
    {
        playerDatasP2.playerName = name;
    }

    public void ChangeIconPlayer(PlayerDatas player)
    {
        for (int i = 0; i < player.playerIcon.Length; i++)
        {
            if (caller.GetComponent<Image>().sprite == player.playerIcon[i])
            {
                player.playerIconID = i;
                break;
            }
        }

    }

    public void SetCaller(BaseEventData callGO)
    {
        PointerEventData pointer = callGO as PointerEventData;
        caller = pointer.pointerCurrentRaycast.gameObject;
    }




}
