﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class CameraRotation : MonoBehaviour
{
    [SerializeField] private Camera cam;
    [SerializeField] private Ease rotationEase;


    public void RotateCamera(float time, Vector3 angle)
    {
        cam.transform.DORotate(angle, time, RotateMode.LocalAxisAdd).SetEase(rotationEase);
    }

    public void RotateCamera(Vector3 angle)
    {
        cam.transform.localEulerAngles = angle;
    }
}
