﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityAtoms.BaseAtoms;
using UnityEngine.UI;
public class NetworkPlayerManager : MonoBehaviourPunCallbacks, IPunObservable
{
    [SerializeField] private IntVariable valuePlayer;
    [SerializeField] private IntVariable numberPlayer;
    [SerializeField] private BoolVariable startGame;
    [SerializeField] private BoolVariable canShoot;
    [SerializeField] private IntVariable turnValue;
    [SerializeField] private UnityEngine.Events.UnityEvent _OnStart;
    [SerializeField] private UnityEngine.Events.UnityEvent _OnInitialize;
    [SerializeField] private Text text;
    private bool isStarted = true;
    private bool onceStart = false;
    private IEnumerator Start()
    {
        yield return new WaitForSeconds(.2f);
        _OnStart.Invoke();
        yield return new WaitForSeconds(2);
        isStarted = false;
    }

    private void Update()
    {
        text.text = "ValuePlayer : " + valuePlayer.Value + " TurnValue : " + turnValue.Value + "CanShoot: " + canShoot.Value;
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (isStarted)
        {
            if (stream.IsWriting)
            {
                
                if (!onceStart)
                {
                    _OnInitialize.Invoke();
                    onceStart = true;
                }
                stream.SendNext(valuePlayer.Value);
                stream.SendNext(numberPlayer.Value);
                stream.SendNext(startGame.Value = true);
                stream.SendNext(canShoot.Value);
                

            }

            else if (stream.IsReading)
            {
                if ((int)stream.ReceiveNext() == 1)
                {
                    valuePlayer.Value = 2;


                }
                else
                {
                    valuePlayer.Value = 1;

                }
                numberPlayer.Value = (int)stream.ReceiveNext() * -1;
                startGame.Value = (bool)stream.ReceiveNext();


                if ((bool)stream.ReceiveNext() == false)
                {
                    canShoot.Value = true;
                }
                else
                {
                    canShoot.Value = false;
                }
            }
        }

        
    }


}
