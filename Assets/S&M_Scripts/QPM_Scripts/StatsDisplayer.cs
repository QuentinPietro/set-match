﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatsDisplayer : MonoBehaviour
{
    [SerializeField] private PlayerDatas player;

    [SerializeField] private Text playedTime;
    [SerializeField] private Text playedMatchs;
    [SerializeField] private Text wins;
    [SerializeField] private Text looses;
    [SerializeField] private Text winRate;
    [SerializeField] private Text bestRank;
    [SerializeField] private Text bestELO;
    [SerializeField] private Text ELO;
    [SerializeField] private Text longestWinStreak;
    [SerializeField] private Text longestLooseStreak;

    private void Update()
    {
        SetTextes();
    }

    

    private void SetTextes()
    {
        SetPlayedTime();
        playedTime.text ="Played Time : " + Mathf.Round((player.playedTime / 3600)).ToString() + " Hours ";
        playedMatchs.text = "Match Played : " +  player.playedMatchs.ToString();
        wins.text = "Wins : " +  player.wins.ToString();
        looses.text ="Looses : " + player.looses.ToString();
        if (player.playedMatchs > 0)
        {
            winRate.text = "Win Rate : " + player.winRate.ToString() + " %";
        }
        else
        {
            winRate.text = "Win Rate : " + "0" + " %";
        }

        bestRank.text ="Best Rank : " + player.bestRank.ToString();
        bestELO.text ="Best ELO " +  player.bestELO.ToString();
        ELO.text ="ELO " +  player.ELO.ToString();
        longestWinStreak.text ="Longest Wins Streak " + player.winsStreak.ToString();
        longestLooseStreak.text ="Longest Looses Streak " + player.loosesStreak.ToString();

    }

    private void SetPlayedTime()=> player.playedTime = player.lastPlayedTime + (int)Time.time;

    private void OnApplicationQuit()
    {
        player.lastPlayedTime = player.playedTime;
    }
}
