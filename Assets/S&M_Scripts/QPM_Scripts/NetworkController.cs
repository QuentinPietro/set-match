﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
public class NetworkController : MonoBehaviourPunCallbacks
{

    [SerializeField] private PlayerDatas playerDatas;

    private void Start()
    {

        PhotonNetwork.ConnectUsingSettings();
    }

    public void Login()
    {
        //PhotonNetwork.AuthValues = new AuthenticationValues();
        //PhotonNetwork.AuthValues.AuthType = CustomAuthenticationType.Custom;
        //PhotonNetwork.AuthValues.AddAuthParameter("username", playerDatas.playerName);
        //PhotonNetwork.AuthValues.AddAuthParameter("password", playerDatas.password);

    }

    public override void OnConnectedToMaster()
    {
        Debug.Log("We are now connected to the " + PhotonNetwork.CloudRegion + " server!");
        PhotonNetwork.AutomaticallySyncScene = true;
        //Debug.Log(PhotonNetwork.AuthValues.ToString());
        
        //PhotonNetwork.NickName = PlayerPrefs.GetString("NickName");


    }



}
