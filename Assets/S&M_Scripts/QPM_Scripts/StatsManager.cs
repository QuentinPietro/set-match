﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class StatsManager : MonoBehaviour
{
    [SerializeField] private PlayerDatas player;
    [SerializeField] private PlayerDatas opponent;
    [Range(0, 10)]
    [SerializeField] private float k;
    [SerializeField] private float scale;
    [SerializeField] private Text eloText;
    [SerializeField] private Text rankText;

    private void Start() => CalculateStats();
    public void CalculateStats()
    {
        SetWinsAndLoosesStreak();
        SetPlayedMatchs();
        SetELO();
        SetWinsAndLooses();
        SetBestELO();
        SetBestRank();
        SetWinRate();
    }

    private void SetWinRate() => player.winRate = player.wins * 100 / player.playedMatchs;

    private void SetWinsAndLoosesStreak()
    {
        if (player.result == 0)
        {
            player.currentLoosesStreak++;
            player.currentWinsStreak = 0;
            if (player.currentLoosesStreak > player.loosesStreak)
            {
                player.loosesStreak++;
            }
        }
        else
        {
            player.currentWinsStreak++;
            player.currentLoosesStreak = 0;

            if (player.currentWinsStreak > player.winsStreak)
            {
                player.winsStreak++;
            }

        }

    }

    private void SetWinsAndLooses()
    {
        if (player.result == 1) player.wins++;
        else player.looses++;
    }

    private void SetELO()
    {
        player.ELO = player.ELO + k * (player.result - (1f / (1f + Mathf.Pow(10f, (opponent.ELO - player.ELO) / scale))));
        player.ELO = Mathf.Round(player.ELO * 10) / 10;
        if (player.ELO < 0) player.ELO = 0;
        else if (player.ELO > 100) player.ELO = 100;

        if (player.ELO < 10)
        {
            player.rank = PlayerDatas.Rank.Amator;
        }
        else if (player.ELO >= 10 && player.ELO < 22)
        {
            player.rank = PlayerDatas.Rank.Pro;
        }
        else if (player.ELO >= 22 && player.ELO < 34)
        {
            player.rank = PlayerDatas.Rank.Fifth;
        }
        else if (player.ELO >= 34 && player.ELO < 46)
        {
            player.rank = PlayerDatas.Rank.Fourth;
        }
        else if (player.ELO >= 46 && player.ELO < 58)
        {
            player.rank = PlayerDatas.Rank.Third;
        }
        else if (player.ELO >= 58 && player.ELO < 70)
        {
            player.rank = PlayerDatas.Rank.Second;
        }
        else if (player.ELO >= 70 && player.ELO < 82)
        {
            player.rank = PlayerDatas.Rank.First;
        }
        else if (player.ELO >= 82 && player.ELO < 94)
        {
            player.rank = PlayerDatas.Rank.Master;
        }
        else if (player.ELO >= 94)
        {
            player.rank = PlayerDatas.Rank.Legend;
        }

        eloText.text = player.ELO.ToString();
        rankText.text = player.rank.ToString();
    }

    private void SetPlayedMatchs() => player.playedMatchs++;

    private void SetBestELO()
    {
        if (player.ELO > player.bestELO)
        {
            player.bestELO = player.ELO;
        }
    }

    private void SetBestRank()
    {
        if (player.rank.GetHashCode() > player.bestRank.GetHashCode())
        {
            player.bestRank = player.rank;
        }
    }

}
