﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastManager : MonoBehaviour
{
    public static bool isSurfaceTouched(Ray ray, RaycastHit hit,  string layerOrTag, bool isTag)
    {
        if (Physics.Raycast(ray, out hit, 500f))
        {
            if (!isTag)
            {
                if (hit.transform.gameObject.layer == LayerMask.NameToLayer(layerOrTag))
                {
                    
                    return true;
                }
                else return false;
            }
            else
            {
                if (hit.transform.gameObject.CompareTag(layerOrTag))
                {
                    return true;
                }
                else return false;
            }

        }
        else return false;
    }

    public static bool isSurfaceTouched<T>(Ray ray, RaycastHit hit ) where T : Component
    {
        if (Physics.Raycast(ray, out hit, 500f))
        {
            if (hit.transform.gameObject.GetComponent<T>())
            {
                
                return true;
            }
            else return false;
        }
        else return false;
    }

}
