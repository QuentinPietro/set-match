﻿
using UnityEngine;
using UnityEngine.Events;

public class Selecter : MonoBehaviour
{
    [SerializeField] private UnityEvent _OnMouseDown = default;
    [SerializeField] private UnityEvent _OnMouseUp = default;
    [SerializeField] private UnityEvent _OnMouseDrag = default;
    [SerializeField] private UnityEvent _OnMouseEnter = default;
    [SerializeField] private UnityEvent _OnMouseExit = default;
    [SerializeField] private UnityEvent _OnMouseOver = default;



    private void OnMouseDown() => _OnMouseDown.Invoke();
    private void OnMouseUp() => _OnMouseUp.Invoke();
    private void OnMouseDrag() => _OnMouseDrag.Invoke();
    private void OnMouseEnter() => _OnMouseEnter.Invoke();
    private void OnMouseExit() => _OnMouseExit.Invoke();
    private void OnMouseOver() => _OnMouseOver.Invoke();

}
