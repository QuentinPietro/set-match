﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using UnityAtoms.BaseAtoms;

public class CounterDisplayer : MonoBehaviour
{
    [SerializeField] private RectTransform scoreMarker;
    [SerializeField] private GameObject scoreCounter;
    [SerializeField] private Transform littleBallTrans;
    [SerializeField] private IntVariable playerNumber;
    [SerializeField] private IntVariable counterValue;
    [SerializeField] private IntVariable pointsScored;
    [SerializeField] private VisualFeedbackManager visualFeedbackManager;
    [SerializeField] private UnityEngine.Events.UnityEvent _OnScoreDisplay;
    [SerializeField] private UnityEngine.Events.UnityEvent _OnScoreDisplayed;
    [SerializeField] private Tween scoreMarkerTween;
    [SerializeField] private Image[] scoreCounterImages;
    [SerializeField] private Text[] scoreCounterTexts;
    [SerializeField] private List<Color> scoreCounterTextsColor;
    [SerializeField] private List<Color> scoreCounterImagesColors;

    private void Start()
    {
        SetColorList(scoreCounterImages, scoreCounterImagesColors);
        SetColorList(scoreCounterTexts, scoreCounterTextsColor);

    }

    public void DisplayScore()
    {
        _OnScoreDisplay.Invoke();
        scoreMarkerTween = scoreMarker.DOLocalMoveY(( 155 * -counterValue.Value), 1f);
        if (Mathf.Abs(counterValue.Value) > 15)
        {
            visualFeedbackManager.DisplayText("ACE");
        }
        else
        {
            visualFeedbackManager.DisplayText(pointsScored.Value.ToString());
            Debug.Log(pointsScored.Value.ToString());
        }
        
        StartCoroutine(WaitEndOfTween());
        
    }

    IEnumerator WaitEndOfTween()
    {
        yield return scoreMarkerTween.WaitForCompletion();
        yield return new WaitForSeconds(1);

        _OnScoreDisplayed.Invoke();
        yield break;
    }

    void ChangeColorToInitial(Image[] arrayOfElements, List<Color> listOfColor, float time)
    {
            for (int i = 0; i < arrayOfElements.Length; i++)
            {
                arrayOfElements[i].DOColor(listOfColor[i], time);

            }

    }

    void ChangeColorToInitial(Text[] arrayOfElements, List<Color> listOfColor, float time)
    {
        for (int i = 0; i < arrayOfElements.Length; i++)
        {
            arrayOfElements[i].DOColor(listOfColor[i], time);

        }

    }

    void ChangeColorToFade(Image[] arrayOfElements, List<Color> listOfColor, float amountOfFade, float time)
    {
        for (int i = 0; i < arrayOfElements.Length; i++)
        {
            arrayOfElements[i].DOColor(new Color(listOfColor[i].r, listOfColor[i].g, listOfColor[i].b, amountOfFade), time);

        }

    }

    void ChangeColorToFade(Text[] arrayOfElements, List<Color> listOfColor, float amountOfFade, float time)
    {
        for (int i = 0; i < arrayOfElements.Length; i++)
        {
            arrayOfElements[i].DOColor(new Color(listOfColor[i].r, listOfColor[i].g, listOfColor[i].b, amountOfFade), time);

        }

    }

    void SetColorList(Image[] arrayOfElements, List<Color> listOfColor)
    {
        for (int i = 0; i < arrayOfElements.Length; i++)
        {
            listOfColor.Add(arrayOfElements[i].color);
        }
    }

    void SetColorList(Text[] arrayOfElements, List<Color> listOfColor)
    {
        for (int i = 0; i < arrayOfElements.Length; i++)
        {
            listOfColor.Add(arrayOfElements[i].color);
        }
    }

    public void ResetCounterDisplayer()
    {
        StartCoroutine(WaitReset());
        
        
    }

    IEnumerator WaitReset()
    {
        yield return new WaitForSeconds(1);
        scoreMarker.localPosition = Vector3.zero;
        yield break;
    }

}
