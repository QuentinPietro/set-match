﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityAtoms.BaseAtoms;
using DG.Tweening;

public class TrainingManager : MonoBehaviour
{
    [SerializeField] private GameObject bigBall;
    [SerializeField] private GameObject littleBall;
    [SerializeField] private GameObject[] collidersTop;
    [SerializeField] private GameObject seletedCollider;
    [SerializeField] private BoolVariable inTraining;
    [SerializeField] private IntVariable turnValue;
    [SerializeField] private IntVariable valuePlayer;
    [SerializeField] private IntVariable numberPlayer;
    [SerializeField] private IntVariable trainingCounter;
    [SerializeField] private BoolVariable canShoot;
    [SerializeField] private Camera cam;
    [SerializeField] private PlayerDatas playerDatas;
    [SerializeField] private Image transitionImage;
    [SerializeField] private Text namePlayer;
    [SerializeField] private Image imagePlayer;
    [SerializeField] private LayerMask mask;
    [SerializeField] private VisualFeedbackManager visualFeedbackManager;
    [SerializeField] private UnityEngine.Events.UnityEvent _OnStart;
    [SerializeField] private UnityEngine.Events.UnityEvent _OnBallIn;
    [SerializeField] private UnityEngine.Events.UnityEvent _OnBallOut;
    private RaycastHit hit;
    private bool isPlaced;
    private bool canPlace;
    private bool isRandomMod;
    private bool isSelectionMod;
    private bool canSelect;
    private Vector3 bigBallInitialPos;
    private Vector3 littleBallPlacedPos;
    private int random;

    private IEnumerator Start()
    {
        _OnStart.Invoke();
        InitializePlayer();
        yield return new WaitForSeconds(2);
        InitializeTransition();
        
        canShoot.Value = false;
    }
    private void Update()
    {
        if (Input.touchCount > 0 && canPlace)
        {
            if (!isPlaced)
            {
                PlaceTheBall();
            }

        }

        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            if (canSelect)
            {
                SelectArea(true);
            }
        }


    }
    private void ResetRandomMod()
    {
            visualFeedbackManager.DisplayText("Click To Place The Ball");
            bigBall.SetActive(false);
            littleBall.SetActive(false);
            turnValue.Value = 1;
            valuePlayer.Value = 1;
            inTraining.Value = true;
            canShoot.Value = true;
            numberPlayer.Value = 1;
            bigBall.transform.position = bigBallInitialPos;
            isPlaced = false;
            SelectRandomArea(false);

    }
    private void ResetSelectionMod()
    {
        bigBall.transform.position = bigBallInitialPos;
        littleBall.transform.position = littleBallPlacedPos;
        canSelect = true;
        for (int i = 0; i < collidersTop.Length; i++)
        {
            collidersTop[i].transform.parent.gameObject.SetActive(true);
        }

    }
    public void PlaceTheBall()
    {
        if (Physics.Raycast(cam.ScreenPointToRay(Input.GetTouch(0).position), out hit, 1000f, mask))
        {
            littleBall.SetActive(true);
            bigBall.SetActive(true);
            littleBall.transform.position = new Vector3(hit.point.x,0,hit.point.z);
            isPlaced = true;
            //littleBall.transform.position += new Vector3(secondTerrain.position.x - terrain.position.x, 0, 0);
            littleBallPlacedPos = littleBall.transform.position;
            if (isRandomMod)
                SelectRandomArea(true);
            else if (isSelectionMod)
                StartCoroutine(WaitCanSelect());
        }
    }
    public void BallOut()
    {
        _OnBallOut.Invoke();
        StartCoroutine(WaitReset());
    }
    public void BallIn()
    {
        _OnBallIn.Invoke();
        StartCoroutine(WaitReset());
    }
    public void SelectArea(bool value)
    {

        if (value)
        {
            if (Physics.Raycast(cam.ScreenPointToRay(Input.GetTouch(0).position), out hit, 1000f, mask))
            {
                for (int i = 0; i < collidersTop.Length; i++)
                {
                    collidersTop[i].SetActive(false);
                    collidersTop[i].transform.parent.gameObject.SetActive(false);
                    if (hit.collider.transform == collidersTop[i].transform.parent)
                    {
                        collidersTop[i].SetActive(true);
                        collidersTop[i].transform.parent.gameObject.SetActive(true);
                        seletedCollider = collidersTop[i];
                    }

                }
                canShoot.Value = true;
                canSelect = false;
            }

        }
        else
        {
            canShoot.Value = true;
        }
        
    }
    public void SelectRandomArea(bool value)
    {
        if (value)
        {
            random = Random.Range(0, collidersTop.Length);

            for (int i = 0; i < collidersTop.Length; i++)
            {
                collidersTop[i].SetActive(false);
                collidersTop[i].transform.parent.gameObject.SetActive(false);
                if (collidersTop[i].transform.parent == collidersTop[random].transform.parent)
                {
                    collidersTop[i].SetActive(true);
                    collidersTop[i].transform.parent.gameObject.SetActive(true);
                    if (collidersTop[i].transform.parent.GetComponent<BoxCollider>() != null)
                    {
                        collidersTop[i].transform.parent.GetComponent<BoxCollider>().enabled = value;
                    }
                    else
                    {
                        collidersTop[i].transform.parent.GetComponent<MeshCollider>().enabled = value;
                    }
                }
            }

            canShoot.Value = true;
        }
        else
        {
            for (int i = 0; i < collidersTop.Length; i++)
            {
                collidersTop[i].SetActive(value);
                if (collidersTop[i].transform.parent.GetComponent<BoxCollider>() != null)
                {
                    collidersTop[i].transform.parent.GetComponent<BoxCollider>().enabled = value;
                }
                else
                {
                    collidersTop[i].transform.parent.GetComponent<MeshCollider>().enabled = value;
                }

            }
        }

    }
    IEnumerator WaitReset()
    {
        yield return new WaitForSeconds(2f);
        if (isRandomMod)
            ResetRandomMod();
        else if (isSelectionMod)
            ResetSelectionMod();


        InitializeTransition();
    }
    private void InitializeTransition()
    {
        for (int i = 0; i < transitionImage.transform.childCount; i++)
        {
            Image image = transitionImage.transform.GetChild(i).GetComponent<Image>();
            image.DOColor(new Color(image.color.r, image.color.g, image.color.b, 0), .5f);
        }
        transitionImage.DOColor(new Color(transitionImage.color.r, transitionImage.color.g, transitionImage.color.b, 0), .5f);
        transitionImage.raycastTarget = false;
    }
    private void InitializePlayer()
    {
        namePlayer.text = playerDatas.playerName;
        imagePlayer.sprite = playerDatas.playerIcon[playerDatas.playerIconID];

        visualFeedbackManager.DisplayText("Click To Place The Ball");
        bigBall.SetActive(false);
        littleBall.SetActive(false);
        turnValue.Value = 1;
        valuePlayer.Value = 1;
        numberPlayer.Value = 1;
        inTraining.Value = true;
        canShoot.Value = true;
        bigBallInitialPos = bigBall.transform.position;
        bigBall.transform.position = bigBallInitialPos;
        isPlaced = false;

    }
    public void SetRandomMod() { isRandomMod = true; StartCoroutine(WaitCanPlace()); }
    public void SetSelectionMod() { isSelectionMod = true; StartCoroutine(WaitCanPlace()); }
    public void SetCanSelect(bool state) { canSelect = state; }
    private IEnumerator WaitCanPlace()
    {
        yield return new WaitForSeconds(.2f);
        canPlace = true;
        yield break;
    }
    private IEnumerator WaitCanSelect()
    {
        yield return new WaitForSeconds(.2f);
        canSelect = true;
        yield break;
    }

    public void DisplayCounter(Text text)
    {
        text.gameObject.SetActive(true);
        text.text = "Compteur : " + "\n" + trainingCounter.Value.ToString() + " à la suite";

    }

    public void ChangeStateColliderOnFlick()
    {
        if (isSelectionMod)
        {
            for (int i = 0; i < collidersTop.Length; i++)
            {
                collidersTop[i].SetActive(false);
                collidersTop[i].transform.parent.gameObject.SetActive(false);
                seletedCollider.SetActive(true);
                seletedCollider.transform.parent.gameObject.SetActive(true);
            }
        }


    }

}
