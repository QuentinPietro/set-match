﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityAtoms.BaseAtoms;
using Photon.Pun;

public class FlickManager : MonoBehaviour
{
    [SerializeField] private Camera cam;
    [SerializeField] private Rigidbody littleBallRB;
    [SerializeField] private Rigidbody bigBallRB;
    [SerializeField] private IntVariable numberPlayer;
    [SerializeField] private IntVariable counterValue;
    [SerializeField] private SpeedMultiplierValues speedMultiplier;
    [SerializeField] private IntVariable turnValue;
    [SerializeField] private IntVariable valuePlayer;
    [SerializeField] private BoolVariable canShoot;

    [SerializeField] private AreaManager areaManager;

    [SerializeField] private UnityEngine.Events.UnityEvent _OnTerrainTouched;
    [SerializeField] private UnityEngine.Events.UnityEvent _OnBallTouched;
    [SerializeField] private UnityEngine.Events.UnityEvent _AfterFlick;
    [SerializeField] private UnityEngine.Events.UnityEvent _AfterFlickDelayed;

    private RaycastHit hit;

    // Finger Speed;
    private float distance;
    [SerializeField] private float speed;
    private Vector3 vec;

    [SerializeField] private PhotonView photonView;


    void Update()
    {


        if (Input.touchCount > 0)
        {

            FingerDetection();
            

        }
        else
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                if (valuePlayer.Value == turnValue.Value && canShoot.Value)
                {
                    Vector3 newVec = new Vector3();

                    if (PhotonNetwork.InRoom)
                    {
                        newVec = transform.forward * numberPlayer.Value;
                    }
                    else
                    {
                        newVec = transform.forward;
                    }

                    
                    if (PhotonNetwork.InRoom)
                    {
                        photonView.RPC("Flick", RpcTarget.AllBufferedViaServer, newVec, 115f);
                    }
                    else
                    {
                        Flick(newVec, 115f);
                    }
                }
                  
            }
        }


    }

    private void FixedUpdate()
    {
        if (Input.touchCount > 0)
        {
            CalculateFingerSpeedAndDirection();
        }


    }


    void FingerDetection()
    {
        if (RaycastManager.isSurfaceTouched(cam.ScreenPointToRay(Input.GetTouch(0).position), hit, "Terrain", false))
        {
            _OnTerrainTouched.Invoke();
        }
        else if (RaycastManager.isSurfaceTouched(cam.ScreenPointToRay(Input.GetTouch(0).position), hit, "Ball", true))
        {
            if (valuePlayer.Value == turnValue.Value && canShoot.Value)
            {
                _OnBallTouched.Invoke();

                

                if (PhotonNetwork.InRoom)
                {
                    photonView.RPC("Flick", RpcTarget.AllBufferedViaServer, vec * numberPlayer.Value, speed);
                }
                else
                {
                    Flick(vec, speed);
                }

            }

        }
    }

    public void CalculateFingerSpeedAndDirection()
    {
        distance = Input.GetTouch(0).deltaPosition.magnitude;
        speed = distance / Time.deltaTime;
        vec = new Vector3((Input.GetTouch(0).deltaPosition.x), 0, (Input.GetTouch(0).deltaPosition.y));
        vec.Normalize();
    }


    [PunRPC]
    public void Flick(Vector3 vector, float newSpeed)
    {
        bigBallRB.AddForce(vector *  newSpeed * speedMultiplier.speedMultiplier, ForceMode.Impulse);

        littleBallRB.AddForce(vector /** 115f*/ * newSpeed * speedMultiplier.speedMultiplier, ForceMode.Impulse);

        StartCoroutine(WaitEndOfFlick());
    }




    private IEnumerator WaitEndOfFlick()
    {
        
        yield return new WaitForSeconds(.5f);
        yield return new WaitUntil(() => littleBallRB.velocity != Vector3.zero);
        yield return new WaitForSeconds(1f);
        _AfterFlick.Invoke();
        areaManager.CheckAreas(counterValue.Value);

        _AfterFlickDelayed.Invoke();
        //areaManager.photonView.RPC("CheckAreas", RpcTarget.AllBufferedViaServer, counterValue.Value);
        yield break;
    }
}
