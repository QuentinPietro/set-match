﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityAtoms.BaseAtoms;
using UnityEngine.UI;
using Photon.Pun;
public class CounterManager : MonoBehaviour
{
    private Ray rayArea;
    [SerializeField] private IntVariable counterValue;
    [SerializeField] private IntVariable turnValue;
    [SerializeField] private IntVariable pointsScored;
    [SerializeField] private IntVariable numberOfJeux;
    [SerializeField] private Text textNumberOfJeux;
    [SerializeField] private Text textNumberOfJeuxP2;
    [SerializeField] private IntVariable numberOfSets;
    [SerializeField] private Text textNumberOfSets;
    [SerializeField] private Text textNumberOfSetsP2;
    [SerializeField] private CounterDisplayer counterDisplayer;
    [SerializeField] private ResetManager resetManager;
    public MatchCounter matchCounterP1;
    public MatchCounter matchCounterP2;
    [SerializeField] private Text pointsP1Text;
    [SerializeField] private Text pointsP2Text;
    [SerializeField] private Text jeuxP1Text;
    [SerializeField] private Text jeuxP2Text;
    [SerializeField] private Text setsP1Text;
    [SerializeField] private Text setsP2Text;
    [SerializeField] private PhotonView photonView;

    private void Start()
    {
        ChangeTextScore();
        numberOfSets.Value = 3;
        numberOfJeux.Value = 3;
        if (PhotonNetwork.InRoom)
        {
            photonView.RPC("SetUpNumbers", RpcTarget.Others);
        }
    }

    public void ChangeCounterValue( AreasContainer.AreaValue areaValue, int counter)
    {
        switch (areaValue)
        {
            case AreasContainer.AreaValue.Zero:
                if (turnValue.Value == 1)
                {
                    counter += 0;
                }else if (turnValue.Value == 2)
                {
                    counter -= 0;
                }

                pointsScored.Value = 0;
                break;
            case AreasContainer.AreaValue.One:
                if (turnValue.Value == 1)
                {
                    counter += 1;
                }
                else if (turnValue.Value == 2)
                {
                    counter -= 1;
                }

                pointsScored.Value = 1;
                break;
            case AreasContainer.AreaValue.Two:
                if (turnValue.Value == 1)
                {
                    counter += 2;
                }
                else if (turnValue.Value == 2)
                {
                    counter -= 2;
                }

                pointsScored.Value = 2;
                break;
            case AreasContainer.AreaValue.Three:
                if (turnValue.Value == 1)
                {
                    counter += 3;
                }
                else if (turnValue.Value == 2)
                {
                    counter -= 3;
                }

                pointsScored.Value = 3;
                break;
            case AreasContainer.AreaValue.Ace:
                if (turnValue.Value == 1)
                {
                    counter += 20;
                }
                else if (turnValue.Value == 2)
                {
                    counter -= 20;
                }

                break;

        }

        counterValue.Value = counter;

        counterDisplayer.DisplayScore();

        if (counterValue.Value >= 3 || counterValue.Value <= -3)
        {
            CallChangeScore();
        }
        else
        {
            resetManager.ResetDuringExchange();
        }

    }

    public void CallChangeScore()
    {
        if (turnValue.Value == 1) ChangeScore(matchCounterP1, matchCounterP2);
        else if (turnValue.Value == 2) ChangeScore(matchCounterP2, matchCounterP1);
    }

    public void CallChangeScoreMissed()
    {
        if (turnValue.Value == 2) ChangeScore(matchCounterP1, matchCounterP2);
        else if (turnValue.Value == 1) ChangeScore(matchCounterP2, matchCounterP1);
    }

    public void ChangeScore(MatchCounter matchCounter, MatchCounter matchCounter2)
    {


        switch (matchCounter.points)
        {
            case 0:
                matchCounter.points += 15;
                break;
            case 15:
                matchCounter.points += 15;
                break;
            case 30:
                matchCounter.points += 10;
                break;
            case 40:
                matchCounter.points += 10;
                break;

        }

        if (matchCounter.points > 40)
        {
            matchCounter.points = 0;
            matchCounter2.points = 0;
            matchCounter.jeux++;
            if (matchCounter.jeux == numberOfJeux.Value)
            {
                matchCounter.jeux = 0;
                matchCounter2.jeux = 0;
                matchCounter.sets++;
                if (matchCounter.sets == numberOfSets.Value) resetManager.ResetAfterSets();
                else
                {
                    resetManager.ResetAfterJeux();
                }
            }
            else
            {
                resetManager.ResetAfterPoints();
            }
        }
        else
        {
            resetManager.ResetAfterExchange();
        }

        ChangeTextScore();
        
    }

    void ChangeTextScore()
    {
            
            jeuxP1Text.text = matchCounterP1.jeux.ToString();
            pointsP1Text.text = matchCounterP1.points.ToString();
            setsP1Text.text = matchCounterP1.sets.ToString();

            jeuxP2Text.text = matchCounterP2.jeux.ToString();
        pointsP2Text.text = matchCounterP2.points.ToString();
        setsP2Text.text = matchCounterP2.sets.ToString();
    }

    public void ChangeNumberOfJeux(bool value)
    {
        if (value)
        {
            if (numberOfJeux.Value < 10)
            {
                numberOfJeux.Value++;
            }

        }
        else 
        {
            if (numberOfJeux.Value > 1)
            {
                numberOfJeux.Value--;

            }


        }
        if (PhotonNetwork.InRoom)
        {
            photonView.RPC("ChangeNumbers", RpcTarget.Others, numberOfJeux.Value, true);
        }
        textNumberOfJeux.text = numberOfJeux.Value.ToString();
    }
    public void ChangeNumberOfSets(bool value)
    {
        if (value)
        {
            if (numberOfSets.Value < 3)
            {
                numberOfSets.Value++;
            }

        }
        else
        {
            if (numberOfSets.Value > 1)
            {
                numberOfSets.Value--;
                
            }


        }
        if (PhotonNetwork.InRoom)
        {
            photonView.RPC("ChangeNumbers", RpcTarget.Others, numberOfSets.Value, false);
        }
        textNumberOfSets.text = numberOfSets.Value.ToString();
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawRay(rayArea.origin, rayArea.direction * 500);
    }

    [PunRPC]
    private void ChangeNumbers(int number, bool value)
    {
        if (value)
        {
            numberOfJeux.Value = number;
            textNumberOfJeuxP2.text = number.ToString();
        }
        else
        {
            numberOfSets.Value = number;
            textNumberOfSetsP2.text = number.ToString();
        }


    }

    [PunRPC]
    private void SetUpNumbers()
    {
        numberOfJeux.Value = 3;
        numberOfSets.Value = 3;
    }

}
