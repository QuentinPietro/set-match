﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class FadeManager : MonoBehaviour
{

    [SerializeField] private UnityEngine.Events.UnityEvent _OnStart;

    private void Start()
    {
        _OnStart.Invoke();
    }
    public void Fade(Transform _trans)
    {
        FadeProperties(_trans, 0, 1);

        for (int i = 0; i < _trans.childCount; i++)
        {

            FadeProperties(_trans.GetChild(i), 0, 1);

            Fade(_trans.GetChild(i).transform);

        }


    }

    public void UnFade(Transform _trans)
    {
        FadeProperties(_trans, 1, 1);

        for (int i = 0; i < _trans.childCount; i++)
        {
            FadeProperties(_trans.GetChild(i), 1, 1);

            UnFade(_trans.GetChild(i).transform);

        }

    }

    void FadeProperties(Transform _trans, float alpha, float time)
    {
        if (_trans.GetComponent<Image>() != null)
        {
            Color color = _trans.GetComponent<Image>().color;
            _trans.GetComponent<Image>().DOColor(new Color(color.r, color.g, color.b, alpha), time);
        }

        else if (_trans.GetComponent<Text>() != null)
        {
            Color color = _trans.GetComponent<Text>().color;
            _trans.GetComponent<Text>().DOColor(new Color(color.r, color.g, color.b, alpha), time);
        }

        if (_trans.GetComponent<Outline>() != null)
        {
            Color color = _trans.GetComponent<Outline>().effectColor;
            _trans.GetComponent<Outline>().DOColor(new Color(color.r, color.g, color.b, alpha), time);
        }

    }
}
