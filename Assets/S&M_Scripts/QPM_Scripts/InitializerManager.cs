﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityAtoms.BaseAtoms;
using UnityEngine.UI;
using Photon.Pun;
using DG.Tweening;
public class InitializerManager : MonoBehaviour
{
    [SerializeField] UnityEngine.Events.UnityEvent _OnStartDelayed;
    [SerializeField] UnityEngine.Events.UnityEvent _OnStart;
    [SerializeField] private BoolVariable startGame;
    [SerializeField] private IntVariable valuePlayer;
    [SerializeField] private IntVariable numberPlayer;
    [SerializeField] private IntVariable turnValue;
    [SerializeField] private BoolVariable canShoot;
    [SerializeField] private CameraRotation secondCamRot;
    [SerializeField] private Image botImage;
    [SerializeField] private Image topImage;
    [SerializeField] private Image transitionImage;
    [SerializeField] private GameObject gameOptionsP1;
    [SerializeField] private GameObject gameOptionsP2;
    [SerializeField] private GameObject terrain;
    [SerializeField] private Text topText;
    [SerializeField] private Text botText;
    [SerializeField] private PlayerDatas playerDatas;
    [SerializeField] private PlayerDatas playerDatasP2;
    [SerializeField] private PhotonView photonView;
    private IEnumerator Start()
    {
        if (!PhotonNetwork.InRoom) startGame.Value = true;

        _OnStart.Invoke();
        yield return new WaitUntil(() => startGame.Value);
        _OnStartDelayed.Invoke();
        yield return new WaitForSeconds(1);
        StartCoroutine(InitializeDatas());
    }

    public void ChangeCamera()
    {
        if(valuePlayer.Value == 2 )
        {
            secondCamRot.RotateCamera(new Vector3(90, 180, 0));
        }
    }

    public void InitializePlayersValue()
    {

        if (Random.value > .5f)
        {
            valuePlayer.Value = 1;
            numberPlayer.Value = 1;
            canShoot.Value = true;

        }
        else
        {
            valuePlayer.Value = 2;
            numberPlayer.Value = -1;
            canShoot.Value = false;

        }

        turnValue.Value = 1;

    }



    public IEnumerator InitializeDatas()
    {

        if (PhotonNetwork.InRoom)
        {
            photonView.RPC("SetUpPlayerDatasP2", RpcTarget.Others, playerDatas.playerName, playerDatas.playerIconID);
            if (valuePlayer.Value == 1) gameOptionsP1.SetActive(true);
            else gameOptionsP2.SetActive(true);
        }
        
        yield return new WaitForSeconds(2f);
        if (!PhotonNetwork.InRoom)
        {
            canShoot.Value = true;
            valuePlayer.Value = 1;
            numberPlayer.Value = 1;
        }
        

        InitializeNames();
        InitializeImages();

        InitializeTransition();
        transitionImage.raycastTarget = false;
    }

    private void InitializeImages()
    {
        topImage.sprite = playerDatasP2.playerIcon[playerDatasP2.playerIconID];
        botImage.sprite = playerDatas.playerIcon[playerDatas.playerIconID];
    }

    private void InitializeNames()
    {
        topText.text = playerDatasP2.playerName;
        botText.text = playerDatas.playerName;
    }

    [PunRPC]
    void SetUpPlayerDatasP2(string name, int iconID)
    {
        Debug.Log(name);
        playerDatasP2.playerName = name;
        playerDatasP2.playerIconID = iconID;
    }


    private void InitializeTransition()
    {
        for (int i = 0; i < transitionImage.transform.childCount; i++)
        {
            Image image = transitionImage.transform.GetChild(i).GetComponent<Image>();
            image.DOColor(new Color(image.color.r, image.color.g, image.color.b, 0), .5f);
        }
        transitionImage.DOColor(new Color(transitionImage.color.r, transitionImage.color.g, transitionImage.color.b, 0), .5f);
    }

    public void ValidateGameOptions()
    {
        photonView.RPC("PunValidateGameOptions", RpcTarget.All);
    }

    [PunRPC]
    void PunValidateGameOptions()
    {
        gameOptionsP1.SetActive(false);
        gameOptionsP2.SetActive(false);
        terrain.SetActive(true);
    }

}
