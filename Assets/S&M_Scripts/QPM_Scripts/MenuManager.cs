﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class MenuManager : MonoBehaviour
{
    [SerializeField] private GameObject[] selectableButtons;
    [SerializeField] private GameObject[] selectableMenus;
    [SerializeField] private GameObject[] matchMenuButtons;
    [SerializeField] private GameObject[] lockerMenuButtons;
    [SerializeField] private List<bool> matchMenuActivation;
    [SerializeField] private List<bool> lockerMenuActivation;

    private void Start()
    {
        CheckMenus(matchMenuButtons, matchMenuActivation);
        CheckMenus(lockerMenuButtons, lockerMenuActivation);
    }

    public void ChangeMenuButton(GameObject button)
    {
        ResetMenus(matchMenuButtons, matchMenuActivation);
        ResetMenus(lockerMenuButtons, lockerMenuActivation);

        for (int i = 0; i < selectableButtons.Length; i++)
        {
            selectableButtons[i].SetActive(false);
            selectableMenus[i].SetActive(false);
        }

        button.SetActive(true);

    }

    void ResetMenus(GameObject[] array, List<bool> activations)
    {
        for (int i = 0; i < array.Length; i++)
        {
            array[i].SetActive(activations[i]);
        }
    }

    void CheckMenus(GameObject[] array, List<bool> activations)
    {
        for (int i = 0; i < array.Length; i++)
        {
            activations.Add(array[i].activeSelf);
        }
    }


}
