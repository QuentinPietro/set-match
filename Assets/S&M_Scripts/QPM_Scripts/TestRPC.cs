﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
public class TestRPC : MonoBehaviour
{
    private PhotonView photonView;
    public Material redMat;
    public Material blueMat;
    private void Start()
    {
        photonView = GetComponent<PhotonView>();
    }
    void Update()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                photonView.RPC("ChangeColorToRed", RpcTarget.AllBufferedViaServer);
            }

        }
        else
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                photonView.RPC("ChangeColorToBlue", RpcTarget.AllBufferedViaServer);
            }
        }
    }
    
    [PunRPC]
    void ChangeColorToRed()
    {
        
            GetComponent<MeshRenderer>().material = redMat;
        
    }
    
    [PunRPC]
    void ChangeColorToBlue()
    {
        
            GetComponent<MeshRenderer>().material = blueMat;
        
    }
}
