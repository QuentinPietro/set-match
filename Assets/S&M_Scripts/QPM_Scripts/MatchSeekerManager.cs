﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.UI;

public class MatchSeekerManager : MonoBehaviourPunCallbacks
{
    
    [SerializeField]private string randomName;
    [SerializeField] private Text[] roomName;
    [SerializeField] private Text masterClient;
    [SerializeField] private Text nameP2;
    [SerializeField] private Text nameP1;
    [SerializeField] private Image imageP2;
    [SerializeField] private Image imageP1;
    [SerializeField] private GameObject join;
    [SerializeField] private GameObject network;
    [SerializeField] private GameObject rooms;
    [SerializeField] private GameObject roomLobby;
    [SerializeField] private List<RoomInfo> roomListings;
    [SerializeField] private int multiPlayerSceneIndex;
    [SerializeField] private PlayerDatas playerDatasP2;
    [SerializeField] private PlayerDatas playerDatasP1;
    [SerializeField] private PhotonView photonView;
    private int roomNumber;
    private bool isStarted;

    void Start()
    {
        //PhotonNetwork.ConnectUsingSettings();
        roomListings = new List<RoomInfo>();


    }
    private void Update()
    {
        if (network.activeSelf)
        {
            for (int i = 0; i < roomListings.Count; i++)
            {
                roomName[i].text = roomListings[i].PlayerCount + " / " + roomListings[i].MaxPlayers;
            }

            if (PhotonNetwork.InRoom && PhotonNetwork.CurrentRoom.PlayerCount == 2 && !isStarted)
            {
                photonView.RPC("SetUpPlayerDatasP2", RpcTarget.Others, playerDatasP1.playerName, playerDatasP1.playerIconID);
                nameP2.text = playerDatasP2.playerName;
                imageP2.sprite = playerDatasP2.playerIcon[playerDatasP2.playerIconID];

                if (PhotonNetwork.IsMasterClient)
                {
                    StartGame();
                    isStarted = true;
                }
            }


        }
        masterClient.text = PhotonNetwork.IsMasterClient.ToString();

    }


    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {

        Debug.Log("RoomUpdate");
            for (int i = 0; i < roomList.Count; i++)
            {
                roomListings.Add(roomList[i]);
                Debug.Log(roomList[i]);
                Debug.Log("RoomUpdated");
            }

    }
    public void JoinLobby()
    {
        PhotonNetwork.JoinLobby();
    }

    public override void OnJoinedLobby()
    {
        Debug.Log("In Lobby");
        StartCoroutine(CheckOrCreateRoom(roomNumber));
    }

    public IEnumerator CheckOrCreateRoom(int roomNumber)
    {
        yield return new WaitForSeconds(2);
        if (roomListings.Count > 0)
        {
            for (int i = 0; i < roomListings.Count; i++)
            {
                if (roomListings[i].Name == roomNumber.ToString())
                {
                    PhotonNetwork.JoinRoom(roomListings[i].Name);
                    Debug.Log("ROOMEXIST");
                    break;
                }
            }

        }
        else
        {
            RoomOptions roomOps = new RoomOptions() { IsVisible = true, IsOpen = true, MaxPlayers = (byte)2 };
            PhotonNetwork.CreateRoom(roomNumber.ToString(), roomOps);
        }

        
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
      
    }


    public void StartGame()
    {
        if (PhotonNetwork.CurrentRoom.PlayerCount == 2)
        {
            PhotonNetwork.CurrentRoom.IsOpen = false;
            photonView.RPC("LoadLevel", RpcTarget.All);

        }

    }

    public void JoinRoom(Text inputField)
    {
        PhotonNetwork.JoinRoom(inputField.text);
        StartGame();
    }

    public override void OnJoinedRoom()
    {
        Debug.Log(PhotonNetwork.CountOfRooms);
        Debug.Log(PhotonNetwork.CurrentRoom);
        Debug.Log("RoomJoined");

        nameP1.text = playerDatasP1.playerName;
        imageP1.sprite = playerDatasP1.playerIcon[playerDatasP1.playerIconID];
        roomLobby.SetActive(true);
        rooms.SetActive(false);
        roomName[roomNumber].text = roomListings[roomNumber].PlayerCount + " / " + roomListings[roomNumber].MaxPlayers;
    }

    public void SetRoomNumber(int number) => roomNumber = number;

    [PunRPC]
    private void RoomTexts(int number)
    {
        roomName[number].text = roomListings[number].PlayerCount + " / " + roomListings[number].MaxPlayers;
    }

    [PunRPC]
    void SetUpPlayerDatasP2(string name, int iconID)
    {
        Debug.Log(name);
        playerDatasP2.playerName = name;
        playerDatasP2.playerIconID = iconID;
    }

    [PunRPC]
    void LoadLevel()
    {
        PhotonNetwork.LoadLevel(multiPlayerSceneIndex);
    }
}
