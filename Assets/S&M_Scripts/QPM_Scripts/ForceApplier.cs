﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceApplier : MonoBehaviour
{
    public static void ApplyForce(float force, Vector3 direction, ForceMode forceMode, Rigidbody rb)
    {
        rb.AddForce(direction * force, forceMode);
    }
}
