﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BallEditorManager : MonoBehaviour
{
    [SerializeField] private Texture2D texture = default;
    [SerializeField] private Camera cam = default;
    [SerializeField] private SpriteRenderer currentColorSprite = default;
    [SerializeField] private Transform colorWheel = default;
    [SerializeField] private Material ballMat = default;
    [SerializeField] private LayerMask mask = default;
    [SerializeField] private int brushSize = default;
    private Color currentColor = default;
    private RaycastHit2D hit = default;
    private Vector3 currentHitPos = default;
    private Vector2 y1 = default;
    private Vector3 newY2 = default;
    private Vector2 y2 = default;
    private float x1 = default;
    private float x2 = default;
    private Texture2D tex = default;
    private Vector2 sizeTexture = default;
    private void Start()
    {
        ChangeTextureColor(Color.yellow);
    }

    private void Update()
    {
        if (Input.touchCount > 0)
        {

            hit = Physics2D.Raycast(cam.ScreenToWorldPoint(Input.GetTouch(0).position), Vector3.zero, 150f, mask);
            if (hit)
            {
                if (hit.transform == colorWheel)
                {
                    PickColor();
                }
                else
                {
                    ChangePixelsColor();
                }

            }
            texture.IncrementUpdateCount();
            texture.Apply(false);

        }

    }

    private void ChangePixelsColor()
    {
        PickPixel();
        for (int i = 0; i < brushSize; i++)
        {

            for (int j = 0; j < brushSize; j++)
            {
                tex.SetPixel((int)(tex.width * (x1)) + i, (int)(tex.height * x2) + j, currentColor);
                tex.SetPixel((int)(tex.width * (x1)) + i, (int)(tex.height * x2) - j, currentColor);
                tex.SetPixel((int)(tex.width * (x1)) - i, (int)(tex.height * x2) + j, currentColor);
                tex.SetPixel((int)(tex.width * (x1)) - i, (int)(tex.height * x2) - j, currentColor);
                tex.SetPixel((int)(tex.width * (x1)) + j, (int)(tex.height * x2) + i, currentColor);
                tex.SetPixel((int)(tex.width * (x1)) - j, (int)(tex.height * x2) + i, currentColor);
                tex.SetPixel((int)(tex.width * (x1)) + j, (int)(tex.height * x2) - i, currentColor);
                tex.SetPixel((int)(tex.width * (x1)) - j, (int)(tex.height * x2) - i, currentColor);

            }



        }
    }

    private void PickColor()
    {
        PickPixel();
        currentColor = tex.GetPixel((int)(tex.width * x1), (int)(tex.height * x2));
        currentColorSprite.color = currentColor;
    }

    private void PickPixel()
    {
        currentHitPos = cam.WorldToScreenPoint(hit.point);
        y1 = cam.WorldToScreenPoint(hit.transform.position);
        sizeTexture = hit.transform.GetComponent<SpriteRenderer>().size;
        newY2 = hit.collider.transform.position - new Vector3((sizeTexture.x * hit.transform.localScale.x) / 2,
                                                              (sizeTexture.y * hit.transform.localScale.y) / 2,
                                                               0);
        y2 = cam.WorldToScreenPoint(newY2);
        x1 = (-.5f / (y2.x - y1.x)) * currentHitPos.x + ((.5f * y2.x) / (y2.x - y1.x));
        x2 = (-.5f / (y2.y - y1.y)) * currentHitPos.y + ((.5f * y2.y) / (y2.y - y1.y));
        tex = hit.collider.GetComponent<SpriteRenderer>().sprite.texture;
    }
    public void Undo()
    {
        ChangeTextureColor(Color.yellow);
    }

    private void ChangeTextureColor(Color color)
    {
        for (int i = 0; i < texture.width; i++)
        {
            for (int j = 0; j < texture.height; j++)
            {
                texture.SetPixel(i, j, color);
            }
        }

        texture.Apply();
    }
    
    public void Create()
    {
        Texture2D newTex = new Texture2D(1024, 1024,TextureFormat.RGBA32,false);
        Graphics.CopyTexture(texture, newTex);
        ballMat.SetTexture("_MainTex", texture);
        
    }
}
