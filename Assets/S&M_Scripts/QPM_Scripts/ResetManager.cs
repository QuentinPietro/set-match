﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityAtoms.BaseAtoms;
using Photon.Pun;
public class ResetManager : MonoBehaviour
{
    [SerializeField] private GameObject bigBall;
    [SerializeField] private GameObject littleBall;
    [SerializeField] private GameObject fix;
    [SerializeField] private GameObject terrainTop;
    [SerializeField] private GameObject terrainBot;
    [SerializeField] private IntVariable turnValue;
    [SerializeField] private IntVariable valuePlayer;
    [SerializeField] private IntVariable numberPlayer;
    [SerializeField] private IntVariable counterValue;
    [SerializeField] private IntVariable numberOfExchange;
    [SerializeField] private IntVariable currentNumberOfExchange;
    [SerializeField] private CounterDisplayer counterDisplayer;
    [SerializeField] private BoolVariable canShoot;
    [SerializeField] private CameraRotation camRot;
    [SerializeField] private PlayerDatas player;
    [SerializeField] private PlayerDatas opponent;


    [SerializeField] private Transform p1_LeftPosition;
    [SerializeField] private Transform p1_RightPosition;
    [SerializeField] private Transform p2_RightPosition;
    [SerializeField] private Transform p2_LeftPosition;
    [SerializeField] private GameObject p2_LeftSquare;
    [SerializeField] private GameObject p2_RightSquare;
    [SerializeField] private GameObject p1_LeftSquare;
    [SerializeField] private GameObject p1_RightSquare;


    [SerializeField] private UnityEngine.Events.UnityEvent _OnResetDuringExchange;
    [SerializeField] private UnityEngine.Events.UnityEvent _OnResetAfterExchange;
    [SerializeField] private UnityEngine.Events.UnityEvent _OnResetAfterPoints;
    [SerializeField] private UnityEngine.Events.UnityEvent _OnResetAfterJeux;
    [SerializeField] private UnityEngine.Events.UnityEvent _OnResetAfterSets;
    [SerializeField] private UnityEngine.Events.UnityEvent _OnResetGlobal;
    [SerializeField] private UnityEngine.Events.UnityEvent _OnServiceMissed;


    private int turnReminder;
    private Vector3 bigBallInitialPos;

    private IEnumerator Start()
    {
        yield return new WaitForSeconds(1);
        InitializeReset();
    }
    private void Update()
    {

        fix.SetActive(bigBall.activeSelf);
        if (!PhotonNetwork.InRoom)
        {
            valuePlayer.Value = turnValue.Value ;

            if (turnValue.Value == 1)
            {
                numberPlayer.Value = 1;
            }
            else
            {
                numberPlayer.Value = -1;
            }
        }
    }
    public void InitializeReset()
    {
        bigBallInitialPos = bigBall.transform.position;
        bigBall.GetComponent<Rigidbody>().velocity = Vector3.zero;
        turnReminder = 1;
        for (int i = 0; i < terrainBot.transform.childCount; i++)
        {
            terrainBot.transform.GetChild(i).gameObject.SetActive(false);
        }
        numberOfExchange.Value = 0;

        if (turnValue.Value == valuePlayer.Value)
        {
            if (PhotonNetwork.InRoom)
            {
                bigBall.SetActive(true);
                canShoot.Value = true;
            }
            else
            {
                bigBall.SetActive(true);
                canShoot.Value = true;
            }
                
            

        }
        else
        {
            if (PhotonNetwork.InRoom)
            {
                bigBall.SetActive(false);
                canShoot.Value = false;
            }
            else
            {
                bigBall.SetActive(true);
                canShoot.Value = true;
            }



        }

        ChangeLittleBallPosition();
    }

    public void ResetAfterMissedService()
    {
        


        counterValue.Value = 0;
        if (turnValue.Value == valuePlayer.Value)
        {
            if (PhotonNetwork.InRoom)
            {
                bigBall.SetActive(true);
                canShoot.Value = true;
            }
            else
            {
                bigBall.SetActive(true);
                canShoot.Value = true;
            }



        }
        else
        {
            if (PhotonNetwork.InRoom)
            {
                bigBall.SetActive(false);
                canShoot.Value = false;
            }
            else
            {
                bigBall.SetActive(true);
                canShoot.Value = true;
            }


        }

        bigBall.GetComponent<Rigidbody>().velocity = Vector3.zero;
        bigBall.transform.position = bigBallInitialPos;

        currentNumberOfExchange.Value = 0;

        _OnServiceMissed.Invoke();
        ChangeLittleBallPosition();

    }
    public void ResetDuringExchange()
    {
        if (turnValue.Value == 1)
        {
            turnValue.Value = 2;
            
            
            for (int i = 0; i < terrainBot.transform.childCount; i++)
            {
                terrainBot.transform.GetChild(i).gameObject.SetActive(true);
            }
            for (int i = 0; i < terrainTop.transform.childCount; i++)
            {
                terrainTop.transform.GetChild(i).gameObject.SetActive(false);
            }
        }
        else
        {
            turnValue.Value = 1;
            for (int i = 0; i < terrainBot.transform.childCount; i++)
            {
                terrainBot.transform.GetChild(i).gameObject.SetActive(false);
            }
            for (int i = 0; i < terrainTop.transform.childCount; i++)
            {
                terrainTop.transform.GetChild(i).gameObject.SetActive(true);
            }
        }


        if(turnValue.Value == valuePlayer.Value)
        {
            if (PhotonNetwork.InRoom)
            {
                bigBall.SetActive(true);
                canShoot.Value = true;
            }
            else
            {
                bigBall.SetActive(true);
                canShoot.Value = true;
            }



        }
        else
        {
            if (PhotonNetwork.InRoom)
            {
                bigBall.SetActive(false);
                canShoot.Value = false;
            }
            else
            {
                bigBall.SetActive(true);
                canShoot.Value = true;
            }



        }

        bigBall.GetComponent<Rigidbody>().velocity = Vector3.zero;
        bigBall.transform.position = bigBallInitialPos;
        currentNumberOfExchange.Value++;
        _OnResetDuringExchange.Invoke();
    }
    public void ResetAfterExchange() => StartCoroutine(WaitResetAfterExchange());

    public void ResetAfterPoints()
    {
        Debug.Log("Reset After Points");
        if (turnReminder == 1) 
        {
            turnValue.Value = 2;
            turnReminder = 2;
            for (int i = 0; i < terrainBot.transform.childCount; i++)
            {
                terrainBot.transform.GetChild(i).gameObject.SetActive(true);
            }
            for (int i = 0; i < terrainTop.transform.childCount; i++)
            {
                terrainTop.transform.GetChild(i).gameObject.SetActive(false);
            }
            Debug.Log(turnReminder);

        }
        else if(turnReminder == 2)
        {
            turnValue.Value = 1;
            turnReminder = 1;
            for (int i = 0; i < terrainBot.transform.childCount; i++)
            {
                terrainBot.transform.GetChild(i).gameObject.SetActive(false);
            }
            for (int i = 0; i < terrainTop.transform.childCount; i++)
            {
                terrainTop.transform.GetChild(i).gameObject.SetActive(true);
            }
            Debug.Log(turnReminder);
        }

        if (turnValue.Value == valuePlayer.Value) canShoot.Value = true;
        else canShoot.Value = false;
        counterDisplayer.ResetCounterDisplayer();
        counterValue.Value = 0;

        if (turnValue.Value == valuePlayer.Value)
        {
            if (PhotonNetwork.InRoom)
            {
                bigBall.SetActive(true);
                canShoot.Value = true;
            }
            else
            {
                bigBall.SetActive(true);
                canShoot.Value = true;
            }



        }
        else
        {
            if (PhotonNetwork.InRoom)
            {
                bigBall.SetActive(false);
                canShoot.Value = false;
            }
            else
            {
                bigBall.SetActive(true);
                canShoot.Value = true;
            }


        }

        bigBall.GetComponent<Rigidbody>().velocity = Vector3.zero;
        bigBall.transform.position = bigBallInitialPos;
        numberOfExchange.Value = 0;
        currentNumberOfExchange.Value = 0;
        ChangeLittleBallPosition();
        _OnResetAfterPoints.Invoke();
        _OnResetGlobal.Invoke();
    }

    public void ResetAfterJeux()
    {
        ResetAfterPoints();
        numberOfExchange.Value = 0;
        currentNumberOfExchange.Value = 0;
        _OnResetAfterJeux.Invoke();
        _OnResetGlobal.Invoke();
    }

    public void ResetAfterSets()
    {
        numberOfExchange.Value = 0;
        currentNumberOfExchange.Value = 0;
        if (turnValue.Value == valuePlayer.Value)
        {
            player.result = 1;
            opponent.result = 0;
        }
        else
        {
            player.result = 0;
            opponent.result = 1;

        }
        _OnResetAfterSets.Invoke();
        _OnResetGlobal.Invoke();
    }

    public void Surrender()
    {
        player.result = 0;
        opponent.result = 1;
        _OnResetAfterSets.Invoke();
    }
    public void FakeWin()
    {
        player.result = 1;
        opponent.result = 0;
        _OnResetAfterSets.Invoke();
    }

    IEnumerator WaitResetAfterExchange()
    {
        yield return new WaitForSeconds(1);
        if (turnReminder == 1)
        {
            turnValue.Value = 1;
            for (int i = 0; i < terrainBot.transform.childCount; i++)
            {
                terrainBot.transform.GetChild(i).gameObject.SetActive(false);
            }
            for (int i = 0; i < terrainTop.transform.childCount; i++)
            {
                terrainTop.transform.GetChild(i).gameObject.SetActive(true);
            }
        }
        else if (turnReminder == 2) 
        { 
            turnValue.Value = 2;
            for (int i = 0; i < terrainBot.transform.childCount; i++)
            {
                terrainBot.transform.GetChild(i).gameObject.SetActive(true);
            }
            for (int i = 0; i < terrainTop.transform.childCount; i++)
            {
                terrainTop.transform.GetChild(i).gameObject.SetActive(false);
            }
        }

        counterDisplayer.ResetCounterDisplayer();
        if (PhotonNetwork.InRoom)
        {
            if (turnValue.Value == valuePlayer.Value) canShoot.Value = true;
            else canShoot.Value = false;

        }
        else
        {
            bigBall.SetActive(true);
            canShoot.Value = true;
        }


        counterValue.Value = 0;
        if (turnValue.Value == valuePlayer.Value)
        {
            if (PhotonNetwork.InRoom)
            {
                bigBall.SetActive(true);
                canShoot.Value = true;
            }
            else
            {
                bigBall.SetActive(true);
                canShoot.Value = true;
            }



        }
        else
        {
            if (PhotonNetwork.InRoom)
            {
                bigBall.SetActive(false); 
                canShoot.Value = false;
            }
            else
            {
                bigBall.SetActive(true);
                canShoot.Value = true;
            }


        }

        bigBall.GetComponent<Rigidbody>().velocity = Vector3.zero;
        bigBall.transform.position = bigBallInitialPos;
        numberOfExchange.Value++;
        currentNumberOfExchange.Value = 0;



        ChangeLittleBallPosition();
        _OnResetAfterExchange.Invoke();
        _OnResetGlobal.Invoke();
    }

    public void ChangeLittleBallPosition()
    {
        if (turnValue.Value == 2)
        {
            if (numberOfExchange.Value % 2 == 0)
            {
                littleBall.transform.position = p2_RightPosition.position;
                for (int i = 0; i < terrainBot.transform.childCount; i++)
                {
                    terrainBot.transform.GetChild(i).gameObject.SetActive(false);
                }
                p1_RightSquare.SetActive(true);
            }
            else
            {
                littleBall.transform.position = p2_LeftPosition.position;
                for (int i = 0; i < terrainBot.transform.childCount; i++)
                {
                    terrainBot.transform.GetChild(i).gameObject.SetActive(false);
                }
                p1_LeftSquare.SetActive(true);
            }
            
        }
        else if ( turnValue.Value == 1)
        {
            if (numberOfExchange.Value % 2 == 0)
            {
                littleBall.transform.position = p1_RightPosition.position;
                for (int i = 0; i < terrainTop.transform.childCount; i++)
                {
                    terrainTop.transform.GetChild(i).gameObject.SetActive(false);
                }
                p2_LeftSquare.SetActive(true);
            }
            else
            {
                littleBall.transform.position = p1_LeftPosition.position;
                
                for (int i = 0; i < terrainTop.transform.childCount; i++)
                {
                    terrainTop.transform.GetChild(i).gameObject.SetActive(false);
                }
                p2_RightSquare.SetActive(true);
            }
        }


    }

}
