﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityAtoms.BaseAtoms;
public class SceneLoader : MonoBehaviour
{
    [SerializeField] private FloatVariable loadTime;
    public void LoadScene(string sceneToLoad) => StartCoroutine(WaitLoadScene(sceneToLoad));
    IEnumerator WaitLoadScene(string sceneToLoad)
    {
        yield return new WaitForSeconds(loadTime.Value);
        SceneManager.LoadSceneAsync(sceneToLoad);
        yield break;
    }
}
