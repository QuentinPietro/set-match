﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityAtoms.BaseAtoms;
using Photon.Pun;



    public class Shoot : MonoBehaviour
    {
        [SerializeField] private UnityEngine.Events.UnityEvent _OnDisable;
        [SerializeField] private UnityEngine.Events.UnityEvent _OnShoot;
        [SerializeField] private UnityEngine.Events.UnityEvent _AfterShoot;
        [SerializeField] private UnityEngine.Events.UnityEvent _FirstTouch;

        [SerializeField] private PhotonView photonView;
        
        [SerializeField] private IntVariable playerNumber;
        [SerializeField] private BoolVariable canShoot;

        [SerializeField] private GameObject visualFinger;
        [SerializeField] private GameObject visualStartPoint;
        [SerializeField] private SpeedMultiplierValues speedMultiplier_SO;

        [SerializeField] private Transform[] balls;
        [SerializeField] private LineRenderer line;
        [SerializeField] private LineRenderer littleLine;
        [SerializeField] private Text speedMultiplierText;

        [SerializeField] private float distance;
        [SerializeField] private float speed;
        [SerializeField] private Vector3 vec;

        private bool firstTouch = false;
        private Ray ray;
        private RaycastHit hit;
        private Vector3 bigBallInitialPosition;
        private Vector3 visualFingerInitialPosition;
        private int stateShoot = 1;

        
        private IEnumerator Start()
        {
           
            bigBallInitialPosition = balls[0].transform.position;
            visualFingerInitialPosition = visualFinger.transform.position;
            yield return new WaitForSeconds(.5f);
            Initialize();
        }


        void Update()
        {
            

            if (Input.touchCount == 3)
            {
                SceneReset(1);
            }
            

            if (canShoot.Value)
            {
            FingerDetection();
            }


        }

        
        
         void FingerDetection()
        {
            if (Input.touchCount != 0)
            {

                ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
                if (Physics.Raycast(ray, out hit, 50f))
                {
                    if (hit.transform.gameObject.layer == LayerMask.NameToLayer("Terrain"))
                    {
                        visualStartPoint.transform.position = hit.point;

                        distance = Input.GetTouch(0).deltaPosition.magnitude;
                        if (Input.GetTouch(0).deltaTime != 0)
                            speed = distance / Input.GetTouch(0).deltaTime;
                        vec = new Vector3((Input.GetTouch(0).deltaPosition.x), 0, (Input.GetTouch(0).deltaPosition.y));
                        vec.Normalize();


                        SetUpLines();
                    }

                    else if (hit.transform.gameObject.CompareTag("Ball"))
                    {
                        if (canShoot.Value)
                        {
                            canShoot.Value = false;

                        
                            photonView.RPC("ShootWithImpulse", RpcTarget.AllBufferedViaServer);
                        
                        
                            _OnShoot.Invoke();

                            StartCoroutine(WaitVelocityNull());
                        
                        }
                    }
                
                }

              
                if (!firstTouch)
                {
                    _FirstTouch.Invoke();
                    firstTouch = true;
                }
                
            }

            else if (Input.GetKeyDown(KeyCode.E))
        {
            photonView.RPC("ShootWithImpulseE", RpcTarget.AllBufferedViaServer);
            _OnShoot.Invoke();

            StartCoroutine(WaitVelocityNull());
        }



        }

    [PunRPC]
    private void ShootWithImpulse()
    {

        for (int i = 0; i < balls.Length; i++)
        {
            if (i == 1)
                vec *= stateShoot;
            balls[i].GetComponent<Rigidbody>().AddForce(vec * speed * speedMultiplier_SO.speedMultiplier, ForceMode.Impulse);

        }
    }
        
        [PunRPC]
        private void ShootWithImpulseE()
    {

        for (int i = 0; i < balls.Length; i++)
        {
            if (i == 1)
                vec *= stateShoot;
            balls[i].GetComponent<Rigidbody>().AddForce(new Vector3(0, 0, 115), ForceMode.Impulse);

        }
    }
        public void SceneReset(float time) => StartCoroutine(WaitSceneReset(time));
        IEnumerator WaitSceneReset(float time)
        {
            yield return new WaitForSeconds(time);
            UnityEngine.SceneManagement.SceneManager.LoadScene(1);
            yield break;
        }

        private void SetUpLines()
        {
            visualFinger.transform.LookAt(balls[0].transform);
            visualFinger.transform.position = new Vector3(hit.point.x, 1, hit.point.z);
            line.SetPosition(1, new Vector3(visualFinger.transform.forward.x, 0, visualFinger.transform.forward.z) * 8f);
            littleLine.SetPosition(1, new Vector3(visualFinger.transform.forward.x, 0, visualFinger.transform.forward.z) * 2f * stateShoot);
        }

        private IEnumerator WaitVelocityNull()
        {
            yield return new WaitForSeconds(.5f);
            yield return new WaitUntil(() => balls[0].GetComponent<Rigidbody>().velocity != Vector3.zero);
            yield return new WaitForSeconds(1f);
            _AfterShoot.Invoke();
            yield break;
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawRay(ray.origin, ray.direction * 50f);
            Gizmos.color = Color.yellow;
            Gizmos.DrawRay(balls[0].transform.position, vec*20);


        }

        public void ChangeSpeedMultiplierPlus()
        {
            speedMultiplier_SO.speedMultiplier += .001f;
            speedMultiplierText.text = "Speed Multiplier = " + speedMultiplier_SO.speedMultiplier.ToString();
        }

        public void ChangeSpeedMultiplierMinus()
        {
            speedMultiplier_SO.speedMultiplier -= .001f;
            speedMultiplierText.text = "Speed Multiplier = " + speedMultiplier_SO.speedMultiplier.ToString();
        }

        void Initialize()
        {
            
            speedMultiplierText.text = "Speed Multiplier = " + speedMultiplier_SO.speedMultiplier.ToString();
        }

        public void ResetShoot() 
        {

       
            firstTouch = false;
            visualFinger.transform.position = visualFingerInitialPosition;
            if (stateShoot == 1) stateShoot = -1;
            else if (stateShoot == -1) stateShoot = 1;

            if (playerNumber.Value == 1) playerNumber.Value = -1;
            else playerNumber.Value = 1;
        }

        public void ResetBallPosition()
        {
            balls[0].transform.position = bigBallInitialPosition;
        }

        private void OnDisable()
        {
            _OnDisable.Invoke();
        }


    }



