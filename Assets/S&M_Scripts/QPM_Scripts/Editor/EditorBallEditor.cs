﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
public class EditorBallEditor : EditorWindow
{
    private int numberOfCases;
    private Object sprites;
    private GameObject casesParent;
    [MenuItem("Window/EditorBallEditor")]
    public static void ShowWindow()
    {
        GetWindow<EditorBallEditor>("EditorBallEditor Window");
    }


    [System.Obsolete]
    private void OnGUI()
    {
        numberOfCases = EditorGUILayout.IntField(numberOfCases);
        sprites = EditorGUILayout.ObjectField(sprites, typeof(GameObject)) as GameObject;
        if (GUILayout.Button("Create Editor"))
        {
            CreateEditor();
        }
    }

    private void CreateEditor()
    {
        casesParent = new GameObject("CasesParent");
        for (int i = 0; i < numberOfCases; i++)
        {
            for (int j = 0; j < numberOfCases; j++)
            {
                GameObject sprite = Instantiate(sprites) as GameObject;
                sprite.transform.position = new Vector2(.3f * i, .3f * j);
                sprite.transform.parent = casesParent.transform;
                sprite.name = i.ToString()+','+j.ToString();
            }
            

        }
    }
}
