﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityAtoms.BaseAtoms;

public class EditorResetter : EditorWindow
{
    public Object[] allBools;
    public Object[] allInts;
    public Object[] allMatchCounters;
    public Object[] allPlayerDatas;

    [MenuItem("Window/ResetWindow")]
    public static void ShowWindow()
    {
        GetWindow<EditorResetter>("Reset Window");
    }

    [System.Obsolete]
    private void OnGUI()
    {
        if (GUILayout.Button("Reset All BoolVariables"))
        {
            ResetAtomBool();
        }

        if (GUILayout.Button("Reset All IntVariables"))
        {
            ResetAtomInt();
        }

        if (GUILayout.Button("Reset All MatchCounter"))
        {
            ResetMatchCounter();
        }
        
        if (GUILayout.Button("Reset All PlayerDatas"))
        {
            ResetPlayerDatas();
        }


        if (GUILayout.Button("Reset ALL !"))
        {
            ResetAtomBool();
            ResetAtomInt();
            ResetMatchCounter();
        }
    }
    [System.Obsolete]
    void ResetAtomBool()
    {
        allBools = FindObjectsOfTypeIncludingAssets(typeof(BoolVariable));
        BoolVariable[] allBoolVariables = (BoolVariable[])allBools;
        for (int i = 0; i < allBoolVariables.Length; i++)
        {
            Debug.Log("BOOLVARIABLES ARE RESET");
            allBoolVariables[i].Reset();
        }
    }

    [System.Obsolete]
    void ResetAtomInt()
    {
        allInts = FindObjectsOfTypeIncludingAssets(typeof(IntVariable));
        IntVariable[] allIntsVariables = (IntVariable[])allInts;
        for (int i = 0; i < allIntsVariables.Length; i++)
        {
            Debug.Log("INTVARIABLES ARE RESET");
            allIntsVariables[i].Reset();
        }
    }

    [System.Obsolete]
    void ResetMatchCounter()
    {
        allMatchCounters = FindObjectsOfTypeIncludingAssets(typeof(MatchCounter));
        MatchCounter[] allMatchCounter = (MatchCounter[])allMatchCounters;
        for (int i = 0; i < allMatchCounter.Length; i++)
        {
            Debug.Log("MATCHCOUNTERS ARE RESET");
            allMatchCounter[i].Reset();
        }
    }
    
    [System.Obsolete]
    void ResetPlayerDatas()
    {
        allPlayerDatas = FindObjectsOfTypeIncludingAssets(typeof(PlayerDatas));
        PlayerDatas[] datas = (PlayerDatas[])allPlayerDatas;
        for (int i = 0; i < datas.Length; i++)
        {
            Debug.Log("PLAYERDATAS ARE RESET");
            datas[i].ResetStats();
        }
    }

}
