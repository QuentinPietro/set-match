﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "S&M_Assets/CounterDatas")]
public class MatchCounter : ScriptableObject
{
    public int points;
    public int jeux;
    public int sets;

    public void Reset()
    {
        points = 0;
        sets = 0;
        jeux = 0;
    }
}
