﻿Shader "Unlit/BallEditorShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Position ("WorldPosition", Vector) = (0,0,0,0) 
        _Radius ("SphereRadius", Range(0,100)) = 0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
// Upgrade NOTE: excluded shader from DX11; has structs without semantics (struct appdata members worldPos)
#pragma exclude_renderers d3d11
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
				float3 worldPos;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
			float4 _Position;
			half _Radius;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);
				half grayscale = (col.r+col.g+col.b)*0.333;
				fixed3 c_g = fixed3(grayscale,grayscale,grayscale);

				half d = distance(_Position,i.worldPos);
				half sum = d - _Radius;
				fixed4 lerpColor = lerp(fixed4(c_g,1),c,sum);
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return lerpColor.rgb;
            }
            ENDCG
        }
    }
}
