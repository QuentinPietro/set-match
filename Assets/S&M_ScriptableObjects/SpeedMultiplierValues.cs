﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "S&M_Assets/SpeedMultiplierValues")]
public class SpeedMultiplierValues : ScriptableObject
{
    public float speedMultiplier;
}
