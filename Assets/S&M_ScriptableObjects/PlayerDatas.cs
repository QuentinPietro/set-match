﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if Unity_Editor
using UnityEditor;
#endif

[CreateAssetMenu(menuName = "S&M_Assets/PlayerDatas")]
public class PlayerDatas : ScriptableObject
{
    public enum Rank { Amator, Pro, Fifth, Fourth,Third,Second,First, Master, Legend}

    public string playerName;
    public Sprite[] playerIcon;
    public int playerIconID;
    public bool isConnected;

    [Header("Last Match")]
    public int result;

    [Header("ELO")]
    public float ELO;
    public Rank rank;

    [Header("Stats")]
    public float playedTime;
    public float lastPlayedTime;
    public int playedMatchs;
    public int wins;
    public int looses;
    public int winRate;
    public Rank bestRank;
    public float bestELO;
    public int winsStreak;
    public int loosesStreak;
    public int currentWinsStreak;
    public int currentLoosesStreak;
    public void ResetStats()
    {
        ELO = 0;
        rank = Rank.Amator;

        playedTime = 0;
        lastPlayedTime = 0;
        playedMatchs = 0;
        wins = 0;
        looses = 0;
        winRate = 0;
        bestRank = Rank.Amator;
        bestELO = 0;
        winsStreak = 0;
        loosesStreak = 0;
        currentLoosesStreak = 0;
        currentWinsStreak = 0;
        isConnected = false;
        
    }
}
